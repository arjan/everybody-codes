﻿using System;
using System.Collections.Generic;
using Data;

namespace Search
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			if (args.Length < 1)
				throw new ArgumentException("No query provided");

			string query = args[0];
			ICollection<Camera> cameras = Camera.Load();
			foreach (Camera camera in cameras)
			{
				if (camera.Name.IndexOf(query, StringComparison.OrdinalIgnoreCase) != -1)
					Console.WriteLine(camera);
			}
		}
	}
}
