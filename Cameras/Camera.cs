﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Data
{
	[Serializable]
	public class Camera
	{
		public int Number
		{
			get;
			set;
		}

		public string Name
		{
			get;
			set;
		}

		public float Latitude
		{
			get;
			set;
		}

		public float Longitude
		{
			get;
			set;
		}

		public override string ToString()
		{
			return string.Format("{0} | {1} | {2} | {3}", Number, Name, Latitude, Longitude);
		}

		public static ICollection<Camera> Load()
		{
			return Load("cameras-defb.csv");
		}

		public static ICollection<Camera> Load(string file)
		{
			ICollection<Camera> result = new List<Camera>();
			using (TextReader reader = new StreamReader(file))
			{
				string line = reader.ReadLine(); // ignore header
				while ((line = reader.ReadLine()) != null)
				{
					try
					{
						result.Add(ParseLine(line));
					}
					catch (FormatException)
					{
						// ignore bad line
					}
				}
			}
			return result;
		}

		static Camera ParseLine(string line)
		{
			string[] fields = line.Split(';');
			if (fields.Length != 3)
				throw new FormatException("Invalid line format");
			
			string name = fields[0];
			int number = ParseNumber(name);
			float latitude = float.Parse(fields[1]);
			float longitude = float.Parse(fields[2]);
			return new Camera()
			{
				Number = number,
				Name = name,
				Latitude = latitude,
				Longitude = longitude
			};
		}

		static Regex NumberPattern = new Regex("^\\w+-\\w+-(\\d+)");

		static int ParseNumber(string name)
		{
			Match match = NumberPattern.Match(name);
			if (!match.Success)
				throw new FormatException("Invalid name format");

			string group = match.Groups[1].Value;
			return int.Parse(group);
		}
	}
}

