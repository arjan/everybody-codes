﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Data;

namespace Web.Controllers
{
	public class HomeController : Controller
	{
		IDictionary<string, Camera> data;

		public HomeController()
		{
			data = new Dictionary<string, Camera>();
			foreach (Camera camera in Camera.Load())
			{
				data[camera.Number.ToString()] = camera;
			}
		}

		public ActionResult Index()
		{
			var mvcName = typeof(Controller).Assembly.GetName();
			var isMono = Type.GetType("Mono.Runtime") != null;

			ViewData["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
			ViewData["Runtime"] = isMono ? "Mono" : ".NET";

			return View();
		}

		public JsonResult Cameras()
		{
			return Json(data, JsonRequestBehavior.AllowGet);
		}
	}
}

