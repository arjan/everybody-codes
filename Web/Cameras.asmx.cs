﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Collections.Generic;
using Data;

namespace Web
{
	public class Cameras : WebService
	{
		ICollection<Camera> data;

		public Cameras()
		{
			data = Camera.Load();
		}

		[WebMethod]
		[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
		public List<Camera> GetAll()
		{
			return new List<Camera>(data);
		}
	}
}

