﻿function getId(number) {
	if (number % 15 == 0)
		return 'column15';
	else if (number % 3 == 0)
		return 'column3';
	else if (number % 5 == 0)
		return 'column5';
	else
		return 'columnOther';
}

function showMap(data) {
	var map = L.map('map')
	var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var osmAttrib='Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
	var osm = new L.TileLayer(osmUrl, {attribution: osmAttrib});
	map.setView([52.083, 5.117], 12);
	map.addLayer(osm);
	$.each(data, function (index, element) {
		L.marker([element.Latitude, element.Longitude]).bindPopup(element.Name).addTo(map);
	});
}

$.ajax({ 
    type: 'GET', 
    url: '/Home/Cameras', 
    dataType: 'json',
    success: function (data) { 
        $.each(data, function(index, element) {
            $('#' + getId(element.Number) + ' > tbody').append($('<tr>')
	    		.append($('<td>', { text: element.Number }))
	    		.append($('<td>', { text: element.Name }))
	    		.append($('<td>', { text: element.Latitude }))
	    		.append($('<td>', { text: element.Longitude }))
	    	);
        });
        showMap(data);
    }
});